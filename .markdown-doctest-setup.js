// .markdown-doctest-setup.js
module.exports = {
  require: {
    'edifact_orders_iso_16033/order16033': require('./order16033'),
    'edifact_orders_iso_16033/edi': require('./edi'),
    'lodash': require('lodash'),
    'chai': require('chai'),
  },
  globals: {
    _: require('lodash'),
    'assert': require('assert'),
  }
}