const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-things'));
const util = require('util');


const Orders16033 = require('../order16033.js');

const compact = require('../edi').compact;

describe('test Orders for contacto', function () {
  it('should be able to interpret UNB segment', () => {
    let order = new Orders16033();
    order.interpret(`UNA:+.? 'UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'`);
    expect(order.data.senderIdentification).to.be.eq('WS-ESTORE');
    expect(order.data.recipientIdentification).to.be.eq('NOV');
    expect(order.data.dateOfPreparation).to.be.eq('2018-10-01 12:55:00');
  });

  it('should reject segment that do not respect the norm', () => {
    let order = new Orders16033();
    expect(() => order.interpret(`UNA:+.? 'UNB+UNOC:1+ABCDEFGHIJKLMNOPQRUSTVWXYZ_ABCDEFGHIJKLMNOPQRUSTVWXYZ_+NOV+20181001:1255+253006++ESTORE'`))
      .to.throw('Could not accept ABCDEFGHIJKLMNOPQRUSTVWXYZ_ABCDEFGHIJKLMNOPQRUSTVWXYZ_');
    expect(() => order.interpret(`UNA:+.? 'UNB+UNOC:AAAA+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'`))
      .to.throw('Edifact Parser Error: section=Interchange, segment=UNB (segment #2)  > Error: Invalid character A (65) at position 18');
    expect(() => order.interpret(`UNA:+.? 'UNB+UNOD:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'`))
      .to.throw('Unexpected value: section=Interchange, segment=UNB (segment #2)  > S001 [0]a4 found UNOD but should be UNOC');
    expect(() => order.interpret(`UNA:+.? 'UNB+UNOC:1+WS-ESTORE==>Đ<==+NOV+20181001:1255+253006++ESTORE'`))
      .to.throw('Invalid character Đ (272) at position 32');
  })

  it('should be able to escape chars', () => {
    let order = new Orders16033();
    order.interpret(`UNA:+.? 'UNB+UNOC:1+OPTIC D?'OR?+argent?:bronze??+NOV+20181001:1255+253006++ESTORE'`);
    expect(order.data.senderIdentification).to.be.eq("OPTIC D'OR+argent:bronze?");
    expect(order.data.recipientIdentification).to.be.eq('NOV');
    expect(order.data.dateOfPreparation).to.be.eq('2018-10-01 12:55:00');
  });

  it('should interpret NAD segments', () => {
    const order = new Orders16033();
    order.interpret(compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'
      UNH+140+ORDERS:D:96B:UN:INFO32'
      NAD+BY+26336:100+++WIN OPTIC:31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      `));
    const by = order.data.messages[0].party.BY;
    expect(by.partyId).eq('26336');
    expect(by.codeListQualifier).eq('100');
    expect(by.street).deep.eq(['WIN OPTIC', '31 RUE LOUISE MICHEL']); //TODO vérifier avec Jérôme que c'est bien juste d'avoir le nom du magasin dans street
    expect(by.countryNameCode).eq('FR');
  })

  it('should be able to read order lines', () => {
    let edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'
      UNH+140+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231614'
      LIN+1++1231614+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'`);
    let order = new Orders16033();
    order.interpret(edifact);
    const m = order.data.messages[0];
    expect(m.documentNumber).to.eq('1231614');
    expect(m.lines.length).to.eq(1);
    expect(m.lines[0]).to.have.property('ABG', '14.00');
    expect(m.lines[0]).to.have.property('RAY', '08.60');
    expect(m.lines[0]).to.have.property('SPH', '-0.75');
    expect(m.lines[0]).to.have.property('CYL', '-0.75');
    expect(m.lines[0]).to.have.property('CLX', '090');
    expect(m.lines[0]).to.have.property('quantity', '1');
    expect(m.lines[0]).to.have.property('ItemNumberType', 'SA');
    expect(m.lines[0]).to.have.property('ItemNumber', 'MEL013');
    expect(m.lines[0]).to.have.property('itemDescription1', 'MENICON PREMIO TORIC');
    expect(m.lines[0]).to.have.property('itemDescription2', undefined);
    expect(m.lines[0]).to.have.property('opticianReferenceNumber', '1231614');

  });

  it('should read an example where the products must be delivered directly to the end user', () => {
    const edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'
      UNH+137+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231614'
      DTM+4:201810011221:203'
      NAD+BY+26336:100++WIN OPTIC+31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      CTA+OC+:LEVY MICHAEL'
      COM+0147574678:TE'
      NAD+SU+NOV:100'
      NAD+ST+00000:174++HERRERO UMBERTO+2024 AVENUE DE SOISSONS+CHATEAU-THIERRY++02400+FR'
      CTA+GR'
      COM+0660066099:TE'
      NAD+AA+26336:100'
      LIN+2++1231614+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      LIN+2'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      UNS+S'
      UNT+49+137'
      UNZ+1+253006'`);
    let order = new Orders16033();
    order.interpret(edifact);
    const m = order.data.messages[0];
    expect(m.documentNumber).to.eq('1231614');
    expect(m.lines.length).to.eq(2);
    expect(m.lines[0]).to.have.property('ABG', '14.00');
    expect(m.lines[0]).to.have.property('RAY', '08.60');
    expect(m.lines[0]).to.have.property('SPH', '-0.75');
    expect(m.lines[0]).to.have.property('CYL', '-0.75');
    expect(m.lines[0]).to.have.property('CLX', '090');
    expect(m.lines[0]).to.have.property('quantity', '1');
    expect(m.lines[0]).to.have.property('itemDescription1', 'MENICON PREMIO TORIC');
    expect(m.lines[0]).to.have.property('itemDescription2', undefined);
    expect(m.lines[0]).to.have.property('opticianReferenceNumber', '1231614');
    expect(m.lines[0].party.UD.partyName).to.deep.eq(['HERRERO']);
    expect(m.party.ST).to.deep.eq({
      cityName: "CHATEAU-THIERRY",
      codeListQualifier: "174",
      countryNameCode: "FR",
      countrySubEntityDetails: [""],
      nameAndAddress: [""],
      partyId: "00000",
      partyName: ["HERRERO UMBERTO"],
      postalIdentificationCode: "02400",
      street: ["2024 AVENUE DE SOISSONS"],
      CTA: {
        GR: {
          COM: {
            TE: "0660066099"
          }
        }
      }
    });

    expect(m.party.BY).to.deep.eq({
      partyId: '26336',
      codeListQualifier: '100',
      nameAndAddress: [''],
      partyName: ['WIN OPTIC'],
      street: ['31 RUE LOUISE MICHEL'],
      cityName: 'LEVALLOIS-PERRET',
      countrySubEntityDetails: [''],
      postalIdentificationCode: '92300',
      countryNameCode: 'FR',
      CTA: {
        OC: {
          COM: {
            TE: '0147574678'
          },
          departmentOrEmployee: 'LEVY MICHAEL'
        }
      }
    })
  });

  it('should be able to read an example where the delivery is done to the shop and not directly to the end user', () => {
    const edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20200812:1211+253009++ESTORE'
      UNH+140+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231615'
      DTM+4:202008121211:203'
      NAD+BY+26336:100+++WIN OPTIC:31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      CTA+OC+LEVY MICHAEL'
      COM+0147574678:TE'
      NAD+SU+NOV:100'
      NAD+ST+26336:100'
      NAD+AA+26336:100'
      LIN+2++1231615+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231615'
      NAD+UD+++HERRERO'
      LIN+2'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231615'
      NAD+UD+++HERRERO'
      UNS+S'
      UNT+47+140'
      UNZ+1+253009'
      `)
    let order = new Orders16033();
    order.interpret(edifact);
    const m = order.data.messages[0];
    expect(m.documentNumber).to.eq('1231615');
    expect(m.lines.length).to.eq(2);
    expect(m.lines[0]).to.have.property('ABG', '14.00');
    expect(m.lines[0]).to.have.property('RAY', '08.60');
    expect(m.lines[0]).to.have.property('SPH', '-0.75');
    expect(m.lines[0]).to.have.property('CYL', '-0.75');
    expect(m.lines[0]).to.have.property('CLX', '090');
    expect(m.lines[0]).to.have.property('quantity', '1');
    expect(m.lines[0]).to.have.property('itemDescription1', 'MENICON PREMIO TORIC');
    expect(m.lines[0]).to.have.property('itemDescription2', undefined);
    expect(m.lines[0]).to.have.property('opticianReferenceNumber', '1231615');
    expect(m.lines[0].party.UD.partyName).to.deep.eq(['HERRERO']);
  })

  it('should handle properly multiple COM segment by attaching communication contact to the current CTA of the current NAD', () => {
    const edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'
      UNH+137+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231614'
      DTM+4:201810011221:203'
      NAD+BY+26336:100++WIN OPTIC+31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      CTA+OC+LEVY MICHAEL'
      COM+0147574678:TE'
      NAD+SU+NOV:100'
      NAD+ST+00000:174++HERRERO UMBERTO+2024 AVENUE DE SOISSONS:+CHATEAU-THIERRY++02400+FR'
      CTA+GR'
      COM+0660066099:TE'
      COM+umberto.herrero@gmail.com:EM'
      NAD+AA+26336:100'
      LIN+2++1231614+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      LIN+2'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      UNS+S'
      UNT+50+137'
      UNZ+1+253006'  
      `)
    let order = new Orders16033();
    order.interpret(edifact);
    //console.dir(order.data,{depth:null})
    expect(order.data.messages[0].party.BY.CTA.OC.COM.TE).eq('0147574678');
    expect(order.data.messages[0].party.ST.CTA.GR.COM.TE).eq('0660066099');
    expect(order.data.messages[0].party.ST.CTA.GR.COM.EM).eq('umberto.herrero@gmail.com');

    expect(order.data).to.deep.eq(
      {
        messages: [
          {
            lines: [
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 1,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231614',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              },
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 2,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231614',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              }
            ],
            party: {
              BY: {
                partyId: '26336',
                codeListQualifier: '100',
                nameAndAddress: [''],
                partyName: ['WIN OPTIC'],
                street: ['31 RUE LOUISE MICHEL'],
                cityName: 'LEVALLOIS-PERRET',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '92300',
                countryNameCode: 'FR',
                CTA: {
                  OC: {
                    departmentOrEmployeeIdentification: 'LEVY MICHAEL',
                    COM: { TE: '0147574678' }
                  }
                }
              },
              SU: { partyId: 'NOV', codeListQualifier: '100' },
              ST: {
                partyId: '00000',
                codeListQualifier: '174',
                nameAndAddress: [''],
                partyName: ['HERRERO UMBERTO'],
                street: ['2024 AVENUE DE SOISSONS', ''],
                cityName: 'CHATEAU-THIERRY',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '02400',
                countryNameCode: 'FR',
                CTA: {
                  GR: {
                    COM: { TE: '0660066099', EM: 'umberto.herrero@gmail.com' }
                  }
                }
              },
              AA: { partyId: '26336', codeListQualifier: '100' }
            },
            messageReferenceNumber: '137',
            documentNumber: '1231614',
            dateOfOrder: '2018-10-01 12:21:00'
          }
        ],
        senderIdentification: 'WS-ESTORE',
        recipientIdentification: 'NOV',
        dateOfPreparation: '2018-10-01 12:55:00',
        interchangeReference: '253006',
        applicationReference: 'ESTORE',
        testIndicator: '0'
      }


    )
  })

  it('should handle properly multiple ORDERS messages', () => {
    const edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+WS-ESTORE+NOV+20181001:1255+253006++ESTORE'
      UNH+137+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231614'
      DTM+4:201810011221:203'
      NAD+BY+26336:100++WIN OPTIC+31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      CTA+OC+LEVY MICHAEL'
      COM+0147574678:TE'
      NAD+SU+NOV:100'
      NAD+ST+00000:174++HERRERO UMBERTO+2024 AVENUE DE SOISSONS:+CHATEAU-THIERRY++02400+FR'
      CTA+GR'
      COM+0660066099:TE'
      COM+umberto.herrero@gmail.com:EM'
      NAD+AA+26336:100'
      LIN+2++1231614+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      LIN+2'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231614'
      NAD+UD+++HERRERO'
      UNS+S'
      UNT+50+137'
      UNH+138+ORDERS:D:96B:UN:INFO32'
      BGM+105+1231615'
      DTM+4:201810011221:203'
      NAD+BY+26336:100++WIN OPTIC+31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'
      CTA+OC+LEVY MICHAEL'
      COM+0147574678:TE'
      NAD+SU+NOV:100'
      NAD+ST+00000:174++HERRERO UMBERTO+2024 AVENUE DE SOISSONS:+CHATEAU-THIERRY++02400+FR'
      CTA+GR'
      COM+0660066099:TE'
      COM+umberto.herrero@gmail.com:EM'
      NAD+AA+26336:100'
      LIN+2++1231615+0'
      PIA+1+0+0+0'
      LIN+1'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231615'
      NAD+UD+++HERRERO'
      LIN+2'
      PIA+5+MEL013:SA'
      IMD+F+8+:::MENICON PREMIO TORIC'
      QTY+21:1:CU'
      CCI+++ABO:::ABG'
      MEA+AAE++MM:14.00'
      CCI+++ABO:::RAY'
      MEA+AAE++MM:08.60'
      CCI+++ABO:::SPH'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CYL'
      MEA+AAE++DP:-0.75'
      CCI+++ABO:::CLX'
      MEA+AAE++°:090'
      RFF+FI:3'
      RFF+AEG:1231615'
      NAD+UD+++HERRERO'
      UNS+S'
      UNT+50+138'
      UNZ+2+253006'  
      `)
    let order = new Orders16033();
    order.interpret(edifact);
    //console.dir(order.data,{depth:null})
    expect(order.data.messages[0].party.BY.CTA.OC.COM.TE).eq('0147574678');
    expect(order.data.messages[0].party.ST.CTA.GR.COM.TE).eq('0660066099');
    expect(order.data.messages[0].party.ST.CTA.GR.COM.EM).eq('umberto.herrero@gmail.com');

    expect(order.data).to.deep.eq(
      {
        messages: [
          {
            lines: [
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 1,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231614',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              },
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 2,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231614',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              }
            ],
            party: {
              BY: {
                partyId: '26336',
                codeListQualifier: '100',
                nameAndAddress: [''],
                partyName: ['WIN OPTIC'],
                street: ['31 RUE LOUISE MICHEL'],
                cityName: 'LEVALLOIS-PERRET',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '92300',
                countryNameCode: 'FR',
                CTA: {
                  OC: {
                    departmentOrEmployeeIdentification: 'LEVY MICHAEL',
                    COM: { TE: '0147574678' }
                  }
                }
              },
              SU: { partyId: 'NOV', codeListQualifier: '100' },
              ST: {
                partyId: '00000',
                codeListQualifier: '174',
                nameAndAddress: [''],
                partyName: ['HERRERO UMBERTO'],
                street: ['2024 AVENUE DE SOISSONS', ''],
                cityName: 'CHATEAU-THIERRY',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '02400',
                countryNameCode: 'FR',
                CTA: {
                  GR: {
                    COM: { TE: '0660066099', EM: 'umberto.herrero@gmail.com' }
                  }
                }
              },
              AA: { partyId: '26336', codeListQualifier: '100' }
            },
            messageReferenceNumber: '137',
            documentNumber: '1231614',
            dateOfOrder: '2018-10-01 12:21:00'
          },
          {
            lines: [
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 1,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231615',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              },
              {
                ItemNumber: 'MEL013',
                ItemNumberType: "SA",
                itemDescription1: 'MENICON PREMIO TORIC',
                itemDescription2: undefined,
                lineItemNumber: 2,
                quantity: '1',
                ABG: '14.00',
                RAY: '08.60',
                SPH: '-0.75',
                CYL: '-0.75',
                CLX: '090',
                typeOfLine: '3',
                opticianReferenceNumber: '1231615',
                party: { UD: { nameAndAddress: [''], partyName: ['HERRERO'] } }
              }
            ],
            party: {
              BY: {
                partyId: '26336',
                codeListQualifier: '100',
                nameAndAddress: [''],
                partyName: ['WIN OPTIC'],
                street: ['31 RUE LOUISE MICHEL'],
                cityName: 'LEVALLOIS-PERRET',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '92300',
                countryNameCode: 'FR',
                CTA: {
                  OC: {
                    departmentOrEmployeeIdentification: 'LEVY MICHAEL',
                    COM: { TE: '0147574678' }
                  }
                }
              },
              SU: { partyId: 'NOV', codeListQualifier: '100' },
              ST: {
                partyId: '00000',
                codeListQualifier: '174',
                nameAndAddress: [''],
                partyName: ['HERRERO UMBERTO'],
                street: ['2024 AVENUE DE SOISSONS', ''],
                cityName: 'CHATEAU-THIERRY',
                countrySubEntityDetails: [''],
                postalIdentificationCode: '02400',
                countryNameCode: 'FR',
                CTA: {
                  GR: {
                    COM: { TE: '0660066099', EM: 'umberto.herrero@gmail.com' }
                  }
                }
              },
              AA: { partyId: '26336', codeListQualifier: '100' }
            },
            messageReferenceNumber: '138',
            documentNumber: '1231615',
            dateOfOrder: '2018-10-01 12:21:00'
          },

        ],
        senderIdentification: 'WS-ESTORE',
        recipientIdentification: 'NOV',
        dateOfPreparation: '2018-10-01 12:55:00',
        interchangeReference: '253006',
        applicationReference: 'ESTORE',
        testIndicator: '0'
      }


    )
  });
  it('should be able to detect test messages', () => {
    const edifact = compact(`
      UNA:+.? '
      UNB+UNOC:1+61920085000060+33450670400060+19960105:1054+0000880++ISS++1+1'
      `);
    let order = new Orders16033();
    order.interpret(edifact);
    expect(order.data.testIndicator).eq('1');
  })

  it('should not crash, but report that UNA is badly written',() => {
    const edifact = compact(`
    UNA:+.?'
    UNB+UNOC:1+NOVA2+NOV+201101:113+15077_0000552++NOVASOF2++1'
    UNH+0578332+ORDERS:D:96B:UN:INFO32'
    BGM+105+0000578332'
    DTM+4:202011011134:203'
    NAD+BY+15077:100'
    NAD+SU+NOV:100'
    LIN+1++0000578332+0'
    PIA+1+0+0+0'
    RFF+AEG:3151'
    RFF+FI:3'
    NAD+BV+15077:100'
    NAD+UD+++3151'
    LIN+1+1++1+10+A'
    PIA+5+CIL096:SA'
    IMD+F+8+:::DailiesTotal1'
    QTY+21:1:CU'
    CCI+++ABO:::ABG:SDI'
    MEA+AAE++mm:14.10'
    CCI+++ABO:::RAY'
    MEA+AAE++mm:8.50'
    CCI+++ABO:::SPH'
    MEA+AAE++DP:-5.00'
    UNS+S'
    MOA+116'
    CNT+23:1'
    UNT+25+0578332'
    UNZ+1+15077_0000552'
    `);
    let order = new Orders16033();
    
    expect(()=>order.interpret(edifact))
    .to.throw('Edifact Parser Error: section=Interchange, segment=undefined (segment #1)  > Error: Invalid character : (58) after reading segment name UNA');
  })

  it('should expose every detail of LIN and correct #4',()=>{
    const edifact = compact(`
    UNA:+.? '
    UNB+UNOC:1+NOVA2+NOV+20201101:1134+15077_0000552++NOVASOF2++1'
    UNH+0578332+ORDERS:D:96B:UN:INFO32'
    BGM+105+0000578332'
    DTM+4:202011011134:203'
    NAD+BY+15077:100'
    NAD+SU+NOV:100'
    LIN+1++0000578332+0'
    PIA+1+0+0+0'
    RFF+AEG:3151'
    RFF+FI:3'
    NAD+BV+15077:100'
    NAD+UD+++3151'
    LIN+1+1++1+10+A'
    PIA+5+CIL096:SA'
    IMD+F+8+:::Dailies Total 1'
    QTY+21:1:CU'
    CCI+++ABO:::ABG:SDI'
    MEA+AAE++mm:14.10'
    CCI+++ABO:::RAY'
    MEA+AAE++mm:8.50'
    CCI+++ABO:::SPH'
    MEA+AAE++DP:-5.00'
    UNS+S'
    MOA+116'
    CNT+23:1'
    UNT+25+0578332'
    UNZ+1+15077_0000552'
    `);
    let order = new Orders16033();
    order.interpret(edifact);
    expect(order.data).to.deep.eq(
      {
        messages: [
          {
            lines: [
              {
                lineItemNumber: 1,
                actionCoded: '1',
                subLineIndicator: '1',
                configurationLevel: '10',
                configuration: 'A',
                ItemNumber: 'CIL096',
                ItemNumberType: 'SA',
                itemDescription1: 'Dailies Total 1',
                itemDescription2: undefined,
                quantity: '1',
                ABG: '14.10',
                RAY: '8.50',
                SPH: '-5.00'
              }
            ],
            party: {
              BY: { partyId: '15077', codeListQualifier: '100' },
              SU: { partyId: 'NOV', codeListQualifier: '100' },
              BV: { partyId: '15077', codeListQualifier: '100' },
              UD: { nameAndAddress: [ '' ], partyName: [ '3151' ] }
            },
            messageReferenceNumber: '0578332',
            documentNumber: '0000578332',
            dateOfOrder: '2020-11-01 11:34:00',
            opticianReferenceNumber: '3151',
            typeOfLine: '3'
          }
        ],
        senderIdentification: 'NOVA2',
        recipientIdentification: 'NOV',
        dateOfPreparation: '2020-11-01 11:34:00',
        interchangeReference: '15077_0000552',
        applicationReference: 'NOVASOF2',
        testIndicator: '0'
      }
    );
  })

  it('should correct #4',()=>{
    const edifact = compact(`
    UNA:+.? '
    UNB+UNOC:1+NOVA2+NOV+20201101:1134+15077_0000552++NOVASOF2++1'
    UNH+0578332+ORDERS:D:96B:UN:INFO32'
    BGM+105+0000578332'
    DTM+4:202011011134:203'
    NAD+BY+15077:100'
    NAD+SU+NOV:100'
    LIN+1++0000578332+0'
    LIN+1+1+a:b:c:d+1:x+10+A'
    UNT+8+0578332'
    UNZ+1+15077_0000552'
    `);
    let order = new Orders16033();
    order.interpret(edifact);
    expect(order.data).to.deep.eq(
      {
        messages: [
          {
            lines: [
              {
                lineItemNumber: 1,
                actionCoded: '1',
                itemNumber: 'a',
                itemNumberType: 'b',
                codeListQualifier:'c',
                codeListResponsibleAgency:'d',
                subLineIndicator: '1',
                subLineItemNumber: 'x',
                configurationLevel: '10',
                configuration: 'A',
              }
            ],
            party: {
              BY: { partyId: '15077', codeListQualifier: '100' },
              SU: { partyId: 'NOV', codeListQualifier: '100' },
            },
            messageReferenceNumber: '0578332',
            documentNumber: '0000578332',
            dateOfOrder: '2020-11-01 11:34:00',
          }
        ],
        senderIdentification: 'NOVA2',
        recipientIdentification: 'NOV',
        dateOfPreparation: '2020-11-01 11:34:00',
        interchangeReference: '15077_0000552',
        applicationReference: 'NOVASOF2',
        testIndicator: '0'
      }
    );
  })


})
