<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

-   [Edi][1]
    -   [reset][2]
    -   [writeSegment][3]
        -   [Parameters][4]
        -   [Examples][5]
    -   [writeElements][6]
        -   [Parameters][7]
    -   [writeComponent][8]
        -   [Parameters][9]
    -   [escape][10]
        -   [Parameters][11]
    -   [compact][12]
        -   [Parameters][13]
    -   [date][14]
        -   [Parameters][15]
    -   [datetime][16]
        -   [Parameters][17]
-   [Segment][18]
    -   [Parameters][19]
    -   [val][20]
        -   [Parameters][21]
-   [Message][22]
    -   [Parameters][23]
    -   [reset][24]
    -   [interpret][25]
        -   [Parameters][26]
    -   [expect][27]
        -   [Parameters][28]
    -   [expectVal][29]
        -   [Parameters][30]
    -   [setProp][31]
        -   [Parameters][32]
    -   [openSegment][33]
        -   [Parameters][34]
-   [Order16033][35]
    -   [reset][36]

## Edi

Edi
this class serves as an Edi writer

before writing a new message, you must call the `.reset` method

### reset

reset
this method clear the current edifact string and reset the internal state of Edi

it has to be called before any new message.

### writeSegment

writeSegement
write a segment of name `segment` with a number of elements. 
if an element is a complex element (= with more than one component),
all components should be passed as an array.

If an element must be empty, pass undefined if this is not the last defined element.

#### Parameters

-   `segment`  {string} the name of the segment (3 UpperCase chars)
-   `elements` **...any** {... string | array}

#### Examples

```javascript
const Edi = require('edifact_orders_iso_16033/edi');
//..
let edi = new Edi();
edi.writeSegment('UNB',['UNOC',1],'WS','ESTORE','NOV',['20181001','1255'],'253006',undefined,'ESTORE');
```

### writeElements

for internal use only

#### Parameters

-   `elements`  

### writeComponent

for internal use only

#### Parameters

-   `component`  

### escape

for internal use only

#### Parameters

-   `str`  

### compact

remove the leading,trailing and LF in order to be complient with EDIFACT
 EDIFACT strings are multiline in the test only for readability

#### Parameters

-   `lines`  

### date

return a Date from an edi date str compatible with SQL

#### Parameters

-   `datestr`  

### datetime

convert dt to an datetime string YYYY-MM-DD HH:MM:SS

#### Parameters

-   `dt` **any** can either be an element ['YYYYMMD','HHMM']  or a YYYYMMDDHHMM string

## Segment

Segment
this class represent a segment with all elements / component

### Parameters

-   `segment`  

### val

return the value of a given element/component of the segment
 if the element or component doen't exist, return undefined.

#### Parameters

-   `element` **int** the number of the element of the segment
-   `component` **int** the number of the compenent of the element or undefined if all components must be returned

## Message

Message
The super class for all message interpreters.

Implement the mechanism of message interpretation but does noting in itself
since it has no implementation of any segment

### Parameters

-   `encoding`  
-   `segments`  
-   `elements`  

### reset

reset is called at the beginning of interpret and must clear all previous data.

should be overwritten by the class that extends Message

### interpret

Interpret an EDIFACT Messaage by calling methods for each segment

Errors will be reported by exceptions

#### Parameters

-   `edifact` **[string][37]** the edifact string

### expect

throw an error if variable has not an expected value

variable = the variable to check
values an expected value or an array of expected value. if undefined, doesn't check, just print the message

returns variable if everything ok

#### Parameters

-   `variable`  
-   `values`  
-   `message`  

### expectVal

check that a given component has a given value or is in a given set of values

#### Parameters

-   `elementNumber` **any** of the current segment
-   `componentNumber` **any** in the element
-   `values` **any** either a string or an array of string

### setProp

set a property if the given element / component is defined

#### Parameters

-   `obj` **[object][38]** the object to set a property
-   `property` **[string][37]** the property name
-   `elementNumber` **[number][39]** the element number of the current segment
-   `componentNumber` **[number][39]** the number of the component to look for.
                                    if undefined, the array of components is put into the propertyif either the element or element.component of the \_currentSegment is undefined, nothing is done

### openSegment

understand the previous segment and open a new one in order to collect elements

#### Parameters

-   `segment` **any** name of the segment

## Order16033

**Extends Message**

Order16033
 a parser for ORDERS according to ISO16033

### reset

reset must be call before interpreting a new message.

[1]: #edi

[2]: #reset

[3]: #writesegment

[4]: #parameters

[5]: #examples

[6]: #writeelements

[7]: #parameters-1

[8]: #writecomponent

[9]: #parameters-2

[10]: #escape

[11]: #parameters-3

[12]: #compact

[13]: #parameters-4

[14]: #date

[15]: #parameters-5

[16]: #datetime

[17]: #parameters-6

[18]: #segment

[19]: #parameters-7

[20]: #val

[21]: #parameters-8

[22]: #message

[23]: #parameters-9

[24]: #reset-1

[25]: #interpret

[26]: #parameters-10

[27]: #expect

[28]: #parameters-11

[29]: #expectval

[30]: #parameters-12

[31]: #setprop

[32]: #parameters-13

[33]: #opensegment

[34]: #parameters-14

[35]: #order16033

[36]: #reset-2

[37]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[38]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[39]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number
