const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-things'));


const Edi = require('../edi');

describe('Edi helpers',()=>{
  it('should convert date 20181001',()=>{
    let d = Edi.date('20181001');
    expect(d).eq('2018-10-01');
  });
  it('should be able to convert an Element [date,time]',()=>{
    let d = Edi.datetime(['20181001','1255']);
    expect(d).eq('2018-10-01 12:55:00');
  })
  it('should be able to convert a YYYYMMDDHHMM to a datetime string',()=>{
    expect(Edi.datetime('201810011255')).eq('2018-10-01 12:55:00');
  })

  it('should escape special chars',()=>{
    expect(Edi.escape("all+special'chars:should?be escaped")).eq("all?+special?'chars?:should??be escaped");
  })

  it('should write segment properly',()=>{
    const edi = new Edi();
    edi.writeSegment('NAD','BY',['26336','100'],undefined,"WIN+OPTIC:D'OR?","31 RUE LOUISE MICHEL","LEVALLOIS-PERRET",undefined,92300,'FR');
    expect(edi.edifact).eq("UNA:+.? 'NAD+BY+26336:100++WIN?+OPTIC?:D?'OR??+31 RUE LOUISE MICHEL+LEVALLOIS-PERRET++92300+FR'")
  })

  it('should compact an edifact string that has leading and trailing space and linefeed for readability',()=>{
    expect(Edi.compact(`
      SEG+toto:blabla:trailling space-->'     
      ERR+truc' 
    `)).eq(`SEG+toto:blabla:trailling space-->'ERR+truc'`)
  })
});
  