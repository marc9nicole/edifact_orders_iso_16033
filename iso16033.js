const segments = require("edifact/segments");
const elements = require("edifact/elements");

segments.UNB = { requires: 0, elements: ['S001','S002','S003','S004','0020','S005','0026','0029','0031','0032','0035']};
//UNH
//BGM
//DTM
//NAD
//LIN
segments.PIA ={ requires: 2, elements:['4347','C212','C212','C212','C212','C212']};
// IMD
// QTY
segments.CCI = { requires:0, elements:['7059','C502','C240','4051']};
//MEA

elements['C240'] = { requires: 1, components: ['an..17','an..3','an..3','an..35','an..35']};


elements['S001'] = { requires: 2, components: ['a4','n1']};
elements['S002'] = { requires: 1, components: ['an..35','an..4','an..14']};
elements['S003'] = { requires: 1, components: ['an..35','an..4','an..14']};
elements['S004'] = { requires: 2, components: ['n8','n4']};

elements['0026'] = { requires: 1, components: ['an..14']};
elements['0031'] = { requires: 1, components: ['n1']};
elements['0032'] = { requires: 0, components: ['an..35']};
elements['0035'] = { requires: 1, components: ['n1']};
elements['7059'] = { requires: 0, components: ['an..3']};


module.exports = {segments,elements};
